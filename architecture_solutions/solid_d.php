<?php

interface HttpService {
    public function request(string $url, string $method, array $options = []): void;
}

class XMLHTTPRequestService implements  HttpService {

    public function request(string $url, string $method, array $options = []): void
    {
        // TODO: Implement request() method.
    }
}

class XMLHttpService extends XMLHTTPRequestService {}

class Http {
    private HttpService $service;

    public function __construct(HttpService $xmlHttpService) { }

    public function get(string $url, array $options) {
        $this->service->request($url, 'GET', $options);
    }

    public function post(string $url) {
        $this->service->request($url, 'GET');
    }
}
