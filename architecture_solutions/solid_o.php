<?php

interface ObjectInterface
{
    public function getObjectName();
}

class SomeObject implements ObjectInterface
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getObjectName(): string
    {
        return $this->name;
    }
}

interface ObjectsHandlerInterface
{
    public function handleObject(ObjectInterface $object): string;
}

class ObjectsHandler implements ObjectsHandlerInterface
{
    public function handleObject(ObjectInterface $object): string
    {
        switch ($object) {
            case $object == 'object_1';
                return 'handle_object_1';
            case $object == 'object_2';
                return 'handle_object_2';
            default:
                return 'handle_another_object';
        }
    }
}


class SomeObjectsHandler
{


    public function __construct(
        private readonly ObjectsHandlerInterface $handler)
    {

    }

    public function handleObjects(array $objects): array
    {
        $handlers = [];

        foreach ($objects as $object) {
            $handlers[] = $this->handler->handleObject($object);
        }

        return $handlers;
    }
}

$objects = [
    new SomeObject('object_1'),
    new SomeObject('object_2')
];


$soh = new SomeObjectsHandler(new ObjectsHandler());
$soh->handleObjects($objects);