<?php

interface KeyStorage
{
    public function saveKey(string $key): void;

    public function getKey(): string;
}

class FileKeyStorage implements KeyStorage
{
    private $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function saveKey(string $key): void
    {
        file_put_contents($this->filename, $key);
    }

    public function getKey(): string
    {
        return file_get_contents($this->filename);
    }
}

class DBKeyStorage implements KeyStorage
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function saveKey(string $key): void
    {
        $stmt = $this->pdo->prepare("INSERT INTO keys (value) VALUES (:value)");
        $stmt->bindParam(':value', $key);
        $stmt->execute();
    }

    public function getKey(): string
    {
        $stmt = $this->pdo->query("SELECT value FROM keys ORDER BY id DESC LIMIT 1");
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result['value'] ?? '';
    }
}

class RedisKeyStorage implements KeyStorage
{
    private $redis;

    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    public function saveKey(string $key): void
    {
        $this->redis->set('key', $key);
    }

    public function getKey(): string
    {
        return $this->redis->get('key') ?? '';
    }
}

class Concept
{
    private $client;

    public function __construct(
        private readonly KeyStorage $keyStorage,
    )
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function getUserData()
    {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }

    protected function getSecretKey(): string
    {
        return $this->keyStorage->getKey();
    }
}
