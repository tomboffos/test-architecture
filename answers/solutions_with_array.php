<?php
$array = [
    ["id" => 1, "date" => "12.01.2020", "name" => "test1"],
    ["id" => 2, "date" => "02.05.2020", "name" => "test2"],
    ["id" => 4, "date" => "08.03.2020", "name" => "test4"],
    ["id" => 1, "date" => "22.01.2020", "name" => "test1"],
    ["id" => 2, "date" => "11.11.2020", "name" => "test4"],
    ["id" => 3, "date" => "06.06.2020", "name" => "test3"],
];

// 1 task filter unique values


function getUniqueValues(array $array): array
{
    $unique_array = array_reduce($array, function ($carry, $item) {
        $id = $item['id'];
        if (!isset($carry[$id])) {
            $carry[$id] = $item;
        }
        return $carry;
    }, []);

    return array_values($unique_array);
}

// 2 task sort values
function sortElements(array $array): array
{
    $sortedArray = $array;
    usort($sortedArray, function ($a, $b) {
        return $a['id'] - $b['id'];
    });

    return $sortedArray;
}


// 3 task filter by definite id
function filterArray(array $array, int $id): array
{
    $filteredArray = $array;
    return array_filter($filteredArray, function ($element, $id) {
        return $element['id'] == $id;
    });
}


// 4 task change key
function changeKeys(array $array): array
{
    return array_reduce($array, function ($acc, $item) {
        $acc[$item['name']] = $item['id'];
        return $acc;
    }, []);

}

// 5 task sql with tags and goods
'SELECT goods.id, goods.name
FROM goods
INNER JOIN (
    SELECT goods_id, COUNT(DISTINCT tag_id) AS tag_count
  FROM goods_tags
  GROUP BY goods_id
) AS goods_tag_counts ON goods.id = goods_tag_counts.goods_id
WHERE goods_tag_counts.tag_count = (SELECT COUNT(*) FROM tags)';

// 6 department sql
'SELECT department_id
FROM evaluations
WHERE gender = true AND value > 5
GROUP BY department_id
HAVING COUNT(DISTINCT respondent_id) = (SELECT COUNT(DISTINCT respondent_id) FROM evaluations WHERE gender = true)';